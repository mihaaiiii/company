package com.sda.company.controller;

import com.sda.company.dto.EmployeeCreateDto;
import com.sda.company.dto.EmployeeDisplayDto;
import com.sda.company.service.EmployeeService;
import com.sda.company.util.CustomFakerEmployee;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/employee")
@ControllerAdvice
public class EmployeeController {

    private final EmployeeService employeeService;
    private final CustomFakerEmployee customFakerEmployee;

    @Autowired
    public EmployeeController(EmployeeService employeeService, CustomFakerEmployee customFakerEmployee) {
        this.employeeService = employeeService;
        this.customFakerEmployee = customFakerEmployee;
    }

    @GetMapping("/hello")
    public ResponseEntity<String> hello(){
        return ResponseEntity.ok("Hello Employee");
    }

    @PostMapping("/createEmployee")
    public ResponseEntity<EmployeeDisplayDto> createEmployee(@RequestBody @Valid EmployeeCreateDto employeeCreateDto){
        EmployeeDisplayDto employeeDisplayDto = employeeService.createEmployee(employeeCreateDto);
        return ResponseEntity.ok(employeeDisplayDto);
    }

    @GetMapping("/generateEmployees")
    public ResponseEntity<String> generateEmployees(){
        employeeService.saveAll(customFakerEmployee.generateEmployees());

        return ResponseEntity.ok("Employees were generated");
    }
}
