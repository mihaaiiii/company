package com.sda.company.convertor;

import com.sda.company.dto.EmployeeCreateDto;
import com.sda.company.dto.EmployeeDisplayDto;
import com.sda.company.dto.EmployeeSummaryDto;
import com.sda.company.dto.EmployeeUpdateDto;
import com.sda.company.model.Employee;

public class EmployeeConvertor {

    public static Employee createDtoToEntity(EmployeeCreateDto employeeCreateDto) {
        Employee employee = new Employee();
        employee.setName(employeeCreateDto.getName());
        employee.setPhoneNumber(employeeCreateDto.getPhoneNumber());
        employee.setCnp(employeeCreateDto.getCnp());
        employee.setEmail(employeeCreateDto.getEmail());
        employee.setAddress(employeeCreateDto.getAddress());
        employee.setSalary(employeeCreateDto.getSalary());
        employee.setJobTitle(employeeCreateDto.getJobTitle());

        return employee;
    }

    public static EmployeeDisplayDto entityToDisplayDto(Employee employee) {
        EmployeeDisplayDto employeeDisplayDto = new EmployeeDisplayDto();

        employeeDisplayDto.setId(employee.getId());
        employeeDisplayDto.setName(employee.getName());
        employeeDisplayDto.setPhoneNumber(employee.getPhoneNumber());
        employeeDisplayDto.setCnp(employee.getCnp());
        employeeDisplayDto.setEmail(employee.getEmail());
        employeeDisplayDto.setAddress(employee.getAddress());
        employeeDisplayDto.setSalary(employee.getSalary());
        employeeDisplayDto.setJobTitle(employee.getJobTitle());

        return employeeDisplayDto;
    }


    public static EmployeeSummaryDto entityToSummaryDto(Employee employee) {
        EmployeeSummaryDto employeeSummaryDto = new EmployeeSummaryDto();

        employeeSummaryDto.setId(employee.getId());
        employeeSummaryDto.setName(employee.getName());
        employeeSummaryDto.setCnp(employee.getCnp());

        return employeeSummaryDto;
    }

    public static Employee updateDtoToEntity(EmployeeUpdateDto employeeUpdateDto) {
        Employee employee = new Employee();

        employee.setId(employeeUpdateDto.getId());
        employee.setName(employeeUpdateDto.getName());
        employee.setPhoneNumber(employeeUpdateDto.getPhoneNumber());
        employee.setCnp(employeeUpdateDto.getCnp());
        employee.setEmail(employeeUpdateDto.getEmail());
        employee.setAddress(employeeUpdateDto.getAddress());
        employee.setJobTitle(employeeUpdateDto.getJobTitle());

        return employee;
    }


}
