package com.sda.company.thymeleaf.controller;

import com.sda.company.dto.CompanyDisplayDto;
import com.sda.company.service.CompanyService;
import com.sda.company.thymeleaf.controller.model.GdprConsent;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;
import java.util.List;

@Controller
public class CompanyControllerThymeleaf {

    private final CompanyService companyService;

    public CompanyControllerThymeleaf(@Qualifier ("companyServiceImpl") CompanyService companyService) {
        this.companyService = companyService;
    }

    @RequestMapping (value = "/", method = RequestMethod.GET)
    public String indexPage(Model model, Principal principal) {
        model.addAttribute("welcomeMessage", "Welcome to our company!");
        model.addAttribute("helloUser", "Hello ");
        model.addAttribute("userName", principal.getName());

        return "index";
    }

    @RequestMapping (value = "/start", method = RequestMethod.GET)
    public String start() {
        return "start";
    }

    @RequestMapping (value = "/company", method = RequestMethod.POST)
    public String getWelcomeForm(@ModelAttribute (name = "welcomeForm") GdprConsent gdprConsent, Model model) {
        if (gdprConsent.getName().isBlank()) {
            model.addAttribute("errorMessage", "Please enter your name");
            return "start";
        }
        if (!gdprConsent.isGdprConsent()) {
            model.addAttribute("errorMessage", "If you want to continue, you must accept the GDPR terms");
            return "start";
        }

        return "company";
    }

    @RequestMapping (value = "/showAll", method = RequestMethod.GET)
    public String showAllCompanies(Model model) {
        List<CompanyDisplayDto> companyDisplayDtoList = companyService.findAll();

        model.addAttribute("companyList", companyDisplayDtoList);

        return "companyTable";
    }

    @RequestMapping (value = "/company", method = RequestMethod.GET)
    public String goToHome() {
        return "company";
    }
}
