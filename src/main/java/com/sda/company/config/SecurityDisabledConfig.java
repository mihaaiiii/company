package com.sda.company.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@Profile (value = "without_security")
public class SecurityDisabledConfig {

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.authorizeHttpRequests(auth -> {
            auth.requestMatchers("/api/v1/company/create").permitAll();
            auth.requestMatchers("/api/v1/company/findByName/{name}").permitAll();
            auth.requestMatchers("/api/v1/company/findAll").permitAll();
            auth.requestMatchers("/api/v1/company/generateCompanies").permitAll();
            auth.requestMatchers("/api/v1/company/update").permitAll();
            auth.requestMatchers("/api/v1/company/deleteById/{id}").permitAll();
            auth.requestMatchers("/api/v1/company/findByNameAndEmail/{name}&{email}").permitAll();

            auth.requestMatchers("/api/v1/employee/hello").permitAll();
            auth.requestMatchers("/api/v1/employee/create").permitAll();
            auth.requestMatchers("/api/v1/employee/generateEmployees").permitAll();

            //request auth for thymeleaf
            auth.requestMatchers("/").permitAll();
            auth.requestMatchers("/start").permitAll();
            auth.requestMatchers("/company").permitAll();
            auth.requestMatchers("/showAll").permitAll();

        }).httpBasic();

        httpSecurity.csrf().disable().authorizeHttpRequests()
                .and().cors().disable().authorizeHttpRequests();

        return httpSecurity.build();
    }


}
