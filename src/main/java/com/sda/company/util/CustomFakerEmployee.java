package com.sda.company.util;

import com.github.javafaker.Faker;
import com.sda.company.model.Employee;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.List;
public class CustomFakerEmployee {
    public List<Employee> generateEmployees(){
        List<Employee> employeeList = new ArrayList<>();
        Faker faker = new Faker();

        for (int i = 0; i < 100; i++) {
            Employee employee = new Employee();
            employee.setName(faker.name().name());
            employee.setAddress(faker.address().fullAddress());
            employee.setEmail(faker.bothify("?????##@company.com"));
            employee.setCnp(faker.number().randomNumber(13, true));
            employee.setPhoneNumber(faker.phoneNumber().phoneNumber());

            employeeList.add(employee);

        }

        return employeeList;

    }
}
