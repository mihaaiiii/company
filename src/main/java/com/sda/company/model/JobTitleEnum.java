package com.sda.company.model;

public enum JobTitleEnum {
    DEVELOPER,
    TESTER,
    TEAM_LEAD
}
