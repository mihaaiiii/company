package com.sda.company.exceptions;

public class CompanyException extends RuntimeException {

    public CompanyException(String message) {
        super(message);
    }
}
