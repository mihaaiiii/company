package com.sda.company.service.impl;

import com.sda.company.convertor.CompanyConvertor;
import com.sda.company.dto.CompanyCreateDto;
import com.sda.company.dto.CompanyDisplayDto;
import com.sda.company.dto.CompanySummaryDto;
import com.sda.company.dto.CompanyUpdateDto;
import com.sda.company.exceptions.CompanyException;
import com.sda.company.model.Company;
import com.sda.company.repository.CompanyRepository;
import com.sda.company.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CompanyServiceImpl implements CompanyService {

    private final CompanyRepository companyRepository;

    @Autowired
    public CompanyServiceImpl(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    @Override
    public CompanyDisplayDto createCompany(CompanyCreateDto companyCreateDto) {
        Company company = companyRepository.save(CompanyConvertor.createDtoToEntity(companyCreateDto));

        return CompanyConvertor.entityToDisplayDto(company);
    }

    @Override
    public CompanyDisplayDto findByName(String name) {
        Company company = companyRepository.findByName(name).orElseThrow(() -> new CompanyException("Company " + name + " not found"));

        return CompanyConvertor.entityToDisplayDto(company);
    }

    @Override
    public List<CompanySummaryDto> findAll(Integer pageNumber, Integer pageSize, String sortBy) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(sortBy));

        List<CompanySummaryDto> companySummaryList = new ArrayList<>();
        companyRepository.findAll(pageable).forEach(company -> companySummaryList.add(CompanyConvertor.entityToSummaryDto(company)));

        return companySummaryList;
    }

    @Override
    public List<CompanyDisplayDto> findAll() {

        List<CompanyDisplayDto> companyDisplayDtos = new ArrayList<>();
        companyRepository.findAll().forEach(company -> companyDisplayDtos.add(CompanyConvertor.entityToDisplayDto(company)));

        return companyDisplayDtos;
    }

    @Override
    public void saveAll(List<Company> companyList) {
        companyRepository.saveAll(companyList);
    }

    @Override
    public CompanyDisplayDto updateCompany(CompanyUpdateDto companyUpdateDto) {
        Company company = companyRepository.save(CompanyConvertor.updateDtoToEntity(companyUpdateDto));

        return CompanyConvertor.entityToDisplayDto(company);
    }

    @Override
    public void deleteById(Integer id) {
        companyRepository.deleteById(id);
    }

    @Override
    public CompanyDisplayDto findByNameAndEmail(String name, String email) {
        Company company = companyRepository.findByNameAndEmail(name, email)
                .orElseThrow(() -> new CompanyException("Company whit name " + name + " and email " + email + " not found"));

        return CompanyConvertor.entityToDisplayDto(company);
    }
}
