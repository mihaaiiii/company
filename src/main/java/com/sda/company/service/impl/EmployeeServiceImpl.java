package com.sda.company.service.impl;

import com.sda.company.convertor.EmployeeConvertor;
import com.sda.company.dto.EmployeeCreateDto;
import com.sda.company.dto.EmployeeDisplayDto;
import com.sda.company.dto.EmployeeSummaryDto;
import com.sda.company.dto.EmployeeUpdateDto;
import com.sda.company.exceptions.EmployeeException;
import com.sda.company.model.Employee;
import com.sda.company.repository.EmployeeRepository;
import com.sda.company.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    private final EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public EmployeeDisplayDto createEmployee(EmployeeCreateDto employeeCreateDto) {
        Employee employee = employeeRepository.save(EmployeeConvertor.createDtoToEntity(employeeCreateDto));

        return EmployeeConvertor.entityToDisplayDto(employee);

    }

    @Override
    public EmployeeDisplayDto findByName(String employeeName) {
        Employee employee = employeeRepository.findByName(employeeName).orElseThrow(() -> new EmployeeException("No employee with name " + employeeName + " found!"));

        return EmployeeConvertor.entityToDisplayDto(employee);
    }

    @Override
    public List<EmployeeSummaryDto> findAll() {
        List<EmployeeSummaryDto> employeeSummaryDtoList = new ArrayList<>();
        employeeRepository.findAll().forEach(employee -> employeeSummaryDtoList.add(EmployeeConvertor.entityToSummaryDto(employee)));

        return employeeSummaryDtoList;
    }

    @Override
    public void saveAll(List<Employee> employeeList) {
        employeeRepository.saveAll(employeeList);
    }

    @Override
    public EmployeeDisplayDto updateEmployee(EmployeeUpdateDto employeeUpdateDto) {
        Employee employee = employeeRepository.save(EmployeeConvertor.updateDtoToEntity(employeeUpdateDto));

        return EmployeeConvertor.entityToDisplayDto(employee);
    }

    @Override
    public void deleteById(Integer employeeId) {
        employeeRepository.deleteById(employeeId);
    }

    @Override
    public Employee findEmployeeByName(String employeeName) {
        return employeeRepository.findByName(employeeName).orElseThrow(() -> new EmployeeException("No employee with name " + employeeName + " found!"));
    }
}
