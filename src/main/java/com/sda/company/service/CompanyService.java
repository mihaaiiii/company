package com.sda.company.service;

import com.sda.company.dto.CompanyCreateDto;
import com.sda.company.dto.CompanyDisplayDto;
import com.sda.company.dto.CompanySummaryDto;
import com.sda.company.dto.CompanyUpdateDto;
import com.sda.company.model.Company;

import java.util.List;

public interface CompanyService {
    CompanyDisplayDto createCompany(CompanyCreateDto companyCreateDto);
    CompanyDisplayDto findByName(String name);

    List<CompanySummaryDto> findAll(Integer pageNumber, Integer pageSize, String sortBy);

    List<CompanyDisplayDto> findAll();

     void saveAll(List<Company> companyList);

     CompanyDisplayDto updateCompany(CompanyUpdateDto companyUpdateDto);

     void  deleteById(Integer id);

     CompanyDisplayDto findByNameAndEmail(String name, String email);
}
