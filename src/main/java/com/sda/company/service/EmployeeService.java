package com.sda.company.service;

import com.sda.company.dto.EmployeeCreateDto;
import com.sda.company.dto.EmployeeDisplayDto;
import com.sda.company.dto.EmployeeSummaryDto;
import com.sda.company.dto.EmployeeUpdateDto;
import com.sda.company.model.Employee;

import java.util.List;

public interface EmployeeService {
    EmployeeDisplayDto createEmployee(EmployeeCreateDto employeeCreateDto);

    EmployeeDisplayDto findByName(String employeeName);

    List<EmployeeSummaryDto> findAll();

    void saveAll(List<Employee> employeeList);

    EmployeeDisplayDto updateEmployee(EmployeeUpdateDto employeeUpdateDto);

    void deleteById(Integer employeeId);

    Employee findEmployeeByName(String employeeName);

}
